package com.json.skipandscroll;

import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;

import com.googlecode.androidannotations.annotations.rest.Accept;
import com.googlecode.androidannotations.annotations.rest.Get;
import com.googlecode.androidannotations.annotations.rest.Post;
import com.googlecode.androidannotations.annotations.rest.Rest;
import com.googlecode.androidannotations.api.rest.MediaType;
@Rest(converters= {MappingJacksonHttpMessageConverter.class})
public interface listinterface {
	
//	@Post("http://192.168.1.3/android_webservice_demo/webservice/list/")
	@Post("http://bugletech.com/clients/android_webservice_demo/webservice/list")
	@Accept(MediaType.APPLICATION_JSON)
	Limitless getList(limitskip l);
	
	//@Post("http://192.168.1.3/android_webservice_demo/webservice/list/")
	@Get("http://bugletech.com/clients/android_webservice_demo/webservice/list/")
	@Accept(MediaType.APPLICATION_JSON)
	Limitless getListAll();
}
