package com.json.skipandscroll;

public class limitskip {
	private String limit;
	private String skip;
	public String getLimit() {
		return limit;
	}
	public void setLimit(String limit) {
		this.limit = limit;
	}
	public String getSkip() {
		return skip;
	}
	public void setSkip(String skip) {
		this.skip = skip;
	}
	@Override
	public String toString() {
		return "limitskip [limit=" + limit + ", skip=" + skip + "]";
	}
	
}
