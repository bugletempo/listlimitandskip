
package com.json.skipandscroll;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonPropertyOrder({
    "list_id",
    "title",
    "short_description",
    "image",
    "is_active",
    "is_delete",
    "date_added",
    "date_modified"
})
public class List {

    @JsonProperty("list_id")
    private String listId;
    @JsonProperty("title")
    private String title;
    @JsonProperty("short_description")
    private String shortDescription;
    @JsonProperty("image")
    private String image;
    @JsonProperty("is_active")
    private String isActive;
    @JsonProperty("is_delete")
    private String isDelete;
    @JsonProperty("date_added")
    private String dateAdded;
    @JsonProperty("date_modified")
    private String dateModified;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The listId
     */
    @JsonProperty("list_id")
    public String getListId() {
        return listId;
    }

    /**
     * 
     * @param listId
     *     The list_id
     */
    @JsonProperty("list_id")
    public void setListId(String listId) {
        this.listId = listId;
    }

    /**
     * 
     * @return
     *     The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The shortDescription
     */
    @JsonProperty("short_description")
    public String getShortDescription() {
        return shortDescription;
    }

    /**
     * 
     * @param shortDescription
     *     The short_description
     */
    @JsonProperty("short_description")
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    /**
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * 
     * @return
     *     The isActive
     */
    @JsonProperty("is_active")
    public String getIsActive() {
        return isActive;
    }

    /**
     * 
     * @param isActive
     *     The is_active
     */
    @JsonProperty("is_active")
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * 
     * @return
     *     The isDelete
     */
    @JsonProperty("is_delete")
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * 
     * @param isDelete
     *     The is_delete
     */
    @JsonProperty("is_delete")
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 
     * @return
     *     The dateAdded
     */
    @JsonProperty("date_added")
    public String getDateAdded() {
        return dateAdded;
    }

    /**
     * 
     * @param dateAdded
     *     The date_added
     */
    @JsonProperty("date_added")
    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    /**
     * 
     * @return
     *     The dateModified
     */
    @JsonProperty("date_modified")
    public String getDateModified() {
        return dateModified;
    }

    /**
     * 
     * @param dateModified
     *     The date_modified
     */
    @JsonProperty("date_modified")
    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    @Override
	public String toString() {
		return "List [listId=" + listId + ", title=" + title
				+ ", shortDescription=" + shortDescription + ", image=" + image
				+ ", isActive=" + isActive + ", isDelete=" + isDelete
				+ ", dateAdded=" + dateAdded + ", dateModified=" + dateModified
				+ ", additionalProperties=" + additionalProperties + "]";
	}

	@JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
