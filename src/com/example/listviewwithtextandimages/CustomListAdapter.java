package com.example.listviewwithtextandimages;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.json.skipandscroll.Limitless;
import com.squareup.picasso.Picasso;

public class CustomListAdapter extends BaseAdapter{

	Context context;
	
	ImageView imgListViewC;
	
	TextView txtListViewC;
	
	Limitless list;
	
	List<com.json.skipandscroll.List> listdata;
	
	public CustomListAdapter(Context context,Limitless list) {
		// TODO Auto-generated constructor stub

		this.context = context;

		this.list = list;
		
		listdata = list.getData().getList();
		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.getData().getList().size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.getData().getList().get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		//super.getView(position, convertView, parent);

		
		LinearLayout rl = null;

		LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if(convertView == null){
			rl = (LinearLayout) li.inflate(R.layout.listview_items_images_texts, null);
			convertView = rl;				
		}

		
		ImageView iv = (ImageView)convertView.findViewById(R.id.imgListView);
		//state[0] = android.R.attr.checkable;
		com.json.skipandscroll.List listdata_item = listdata.get(position); 
		if(!listdata_item.getImage().equals("")){
			Picasso.with(context)
            .load("http://192.168.1.27/android_webservice_demo/assets/images/temp/thumb/"+listdata.get(position).getImage())
            .into(iv);
//			System.out.println("Id:"+listdata_item.getListId()+"Image name:"+listdata_item.getImage());
		}
		else
			iv.setImageResource(R.drawable.ic_launcher);


		TextView tv1 = (TextView)convertView.findViewById(R.id.txtListView);
		//			tv.setTypeface(PocketClinicalApplication.MyriadPro);

		tv1.setText(listdata_item.getTitle());

		return convertView;
	}

}
