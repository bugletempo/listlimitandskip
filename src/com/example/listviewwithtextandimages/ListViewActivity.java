package com.example.listviewwithtextandimages;

import java.util.ArrayList;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.UiThread;
import com.googlecode.androidannotations.annotations.rest.RestService;
import com.json.skipandscroll.Limitless;
import com.json.skipandscroll.limitskip;
import com.json.skipandscroll.listinterface;

@EActivity(R.layout.listview_container)
public class ListViewActivity extends Activity {

	@RestService
	listinterface listdata;
	
	ListView lstWithImagAndTextC;
	
	Limitless list;
	
	CustomListAdapter customAdp;
	
	ArrayList<Integer> alFruitsImages;
	
	ArrayList<String> alFruitsNames;
	
	String limit = "5";
	
	int preLast;
	
	limitskip limitskipjson;
	
	boolean scrollstate = false;
	
    @AfterViews
    public void processView() {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.listview_container);
        
        lstWithImagAndTextC = (ListView)findViewById(R.id.lstWithImageAndText);
        
        alFruitsImages = new ArrayList<Integer>();
        
        alFruitsNames = new ArrayList<String>();
        
        alFruitsImages.add(R.drawable.watermelon);
        alFruitsImages.add(R.drawable.mangoes);
        alFruitsImages.add(R.drawable.chikoos);
        alFruitsImages.add(R.drawable.banana);
        alFruitsImages.add(R.drawable.apple);
        alFruitsImages.add(R.drawable.grapes);
        alFruitsImages.add(R.drawable.pomegranate);
        
        alFruitsNames.add("Water Melon");
        alFruitsNames.add("Mangoes");
        alFruitsNames.add("Chickoo");
        alFruitsNames.add("Banana");
        alFruitsNames.add("Apple");
        alFruitsNames.add("Grapes");
        alFruitsNames.add("Pomegranate");
        
        limitskipjson = new limitskip();
        
        getData();
        
        
    }
    /*@UiThread
    public void setProgress(){
    	ProgressBar pb = (ProgressBar)View.findViewById(R.id.progressBar1);
		pb.setIndeterminate(true);
		lstWithImagAndTextC.addFooterView();
    }*/
    @Background
    public void getData(){
    	try{
    	System.out.println("get Data");
    	if(list==null){
    		limitskipjson.setLimit(limit);
    		limitskipjson.setSkip("0");
    		list = listdata.getList(limitskipjson);
    		System.out.println();
    		afterBackground();
    	}
    	else{
    		
    		limitskipjson.setLimit(limit);
    		limitskipjson.setSkip(""+list.getData().getList().size());
    		
    		System.out.println("Limit:"+limit+",skip:"+limitskipjson.getSkip());
    		
    		Limitless list1 = listdata.getList(limitskipjson);
    		
    		System.out.println("line 99");
    		
    		if(list1.getStatus()){
    			
    			/*for(com.json.skipandscroll.List i :list.getData().getList())
    				System.out.println("Ids:"+i.getListId()+",image:"+i.getImage());
    			*/
    			list.getData().getList().addAll(list1.getData().getList());
    			
    			/*for(com.json.skipandscroll.List i :list.getData().getList())
    				System.out.println("Ids:"+i.getListId()+",image:"+i.getTitle());
    			*/
    			System.out.println("lasta data:"+list.getData().getList().get(list.getData().getList().size()-1).getTitle());
    			notifyupdate();
    		}
    	}
//        	list.getData().getList().addAll(list.getData().getList());
        	
//    	System.out.println("json:"+list.getData().getList().size());
//    	System.out.println("size:"+list.getData().getList().size());
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	scrollstate = false;
    }
    
    @UiThread
    public void notifyupdate(){
    	customAdp.notifyDataSetChanged();
    }
    
    @UiThread
    public void afterBackground(){
    	customAdp = new CustomListAdapter(ListViewActivity.this,list);
        lstWithImagAndTextC.setAdapter(customAdp);
        
        lstWithImagAndTextC.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				System.out.println("view"+view.getId());
//				scrollstate = true;
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
//				System.out.println("First VisibleItem:"+firstVisibleItem+",visibleItemCount:"+visibleItemCount);
//				if(scrollstate){
				//paste this line if your list not updated in last 
				//httpheader.set("Connection","Close");
					switch(view.getId()) {
			        case R.id.lstWithImageAndText:     
			             final int lastItem = firstVisibleItem + visibleItemCount;
			           if(lastItem == totalItemCount) {
			              if(preLast!=lastItem){ //to avoid multiple calls for last item
			                Log.d("Last", "Last");
			                preLast = lastItem;
			                getData();
			              }
			           }
					}
//				}
			}
		});
    }
  
}
